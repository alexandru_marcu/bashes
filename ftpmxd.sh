#!/bin/sh
source /opt/cw/recomenv.sh

######################################
#FTP parameters. do not change!
HOST='on.tmstv.com'
USER='mxdmeon'
PASSWD='123gy295'
######################################

######################################
#local parameters
GROOVYSCRIPT="/opt/cw/tools/ftpmxdsh/ConnectorsSlurper.groovy"

FILECONNECTORS="on_maxd_connectors_v22_20160218.xml.gz"
FILEMOVIEKEYWORDS="on_movie_keywords-db_`date -dlast-monday +%Y%m%d`.xml.gz"
FILETVKEYWORDS="on_tv_keywords-db_`date -dlast-monday +%Y%m%d`.xml.gz"

KEYWORDSFOLDER="/contentwise/ImportGracenote/Updated/Keywords"
CONNECTORSFOLDER="/contentwise/ImportGracenote/Updated/Connectors"
JSONFOLDER="/contentwise/ImportGracenote/Updated/JsonReconcile"

CONNECTORSXML="on_maxd_connectors_v22_20160218.xml"
######################################


#moving previously downloaded files (+json reconciliation file) to ../Old.
#/Updated folder will remain updated with the latest files at each run.
mv -f $KEYWORDSFOLDER/* /contentwise/ImportGracenote/Old/Keywords
mv -f $CONNECTORSFOLDER/* /contentwise/ImportGracenote/Old/Connectors
mv -f $JSONFOLDER/* /contentwise/ImportGracenote/Old/JsonReconcile

#ftp script
ftp -nv $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
cd /On2
lcd /contentwise/ImportGracenote/Updated/Keywords
get $FILEMOVIEKEYWORDS
get $FILETVKEYWORDS
cd /On2/maxd
lcd /contentwise/ImportGracenote/Updated/Connectors
get $FILECONNECTORS
quit
END_SCRIPT

#check if downloaded files exist then unzip the .gz files
if [ -f $KEYWORDSFOLDER/$FILEMOVIEKEYWORDS ] then
        echo -e "\nFTP of $FILEMOVIEKEYWORDS successful!!!\n"
        gunzip -f $KEYWORDSFOLDER/$FILEMOVIEKEYWORDS
else echo "FTP of $FILEMOVIEKEYWORDS didn't work."
fi

if [ -f $KEYWORDSFOLDER/$FILETVKEYWORDS ] then
        echo -e "\nFTP of $FILETVKEYWORDS successful!!!\n"
        gunzip -f $KEYWORDSFOLDER/$FILETVKEYWORDS
else echo "FTP of $FILETVKEYWORDS didn't work."
fi

if [ -f $CONNECTORSFOLDER/$FILECONNECTORS ] then
        echo -e "\nFTP of $FILECONNECTORS successful!!!\n"
        gunzip -f $CONNECTORSFOLDER/$FILECONNECTORS
else echo "FTP of $FILECONNECTORS didn't work."
fi

#calling groovy script for output.json generator
groovy $GROOVYSCRIPT $CONNECTORSFOLDER/$CONNECTORSXML $JSONFOLDER/output.json

exit 0
