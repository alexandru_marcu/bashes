#!/bin/bash

set -u

######################
#fill the lines below with parameters

SFTP_DIRECTORY=/opt/sftp/skybrasil/incoming

#check if any file. if files are present, move then to /opt/cw/etl/input/data.
#once ETL finishes, move files to /opt/etl/input/data_old
#if no file are present, send mail.


function sendMail {

        bodyforMail=$1
        from="skybrasil-prod@contentwise.tv"
        to="bogdan.marcu@contentwise.tv"
        subject="ETL missing input data"
        mail -s "$subject" -r "$from" "$to" <<< "$bodyforMail"

}

_now=$(date)
if [ -d $SFTP_DIRECTORY ]; then
                #get the listings of the directory
                inputzipfile=$(ls $SFTP_DIRECTORY | grep zip)
                #echo "$inputzipfile"
                if [ ! -f $SFTP_DIRECTORY/$inputzipfile ]; then
                                errorlog="ETL missing input data at $_now"
                                sendMail "$errorlog"
                fi
fi
