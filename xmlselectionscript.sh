#!/bin/bash

####################################
# DEFINE THE DIRECTORY FOR XML FILES
####################################
SEARCH_DIR='/contentwise/ImportContenuti/SelectionScript/contenuti'


#####################################################
# DEFINE THE DESTINATION DIRECTORY FOR AGED XML FILES
#####################################################
MOVE_DIR='/contentwise/ImportContenuti/SelectionScript/contenuti/AgedContent'

#############################################################
# COMPARE DETERMINES IF AN XML FILE MUST BE MOVED TO MOVE_DIR
############################################################
function compare {

	FILE_NAME=$1
        FILE_DATE=$2
        
	for i in "$SEARCH_DIR"/* 
	do
		filename2=$(basename "$i" .xml)
		OIFS=$IFS
		IFS=$'_'
		arr2=($filename2)
		for it in "${arr2[0]}"
		do
			COMPARE_NAME=${arr2[0]}
			COMPARE_DATE=${arr2[1]}
			if [ "$FILE_NAME" == "$COMPARE_NAME" ]; then
				MAX_DATE=$FILE_DATE
				if [ "$COMPARE_DATE" -gt "$MAX_DATE" ];then
					MAX_DATE=$COMPARE_DATE 
				fi
			fi	
		done
	done

	if [ "$MAX_DATE" -gt "$FILE_DATE"  ]; then
		return 0 
	else 
		return 1
	fi
}
######################################
# SCAN FUNCTION SEARCHES THE XML FILES 
######################################
function scan {

	DIR=$1
	for entry in "$DIR"/*
	do
		if [ -f "$entry" ];then
			file_name=$(basename "$entry" .xml)
			OIFS=$IFS
			IFS=$'_'
      	
			arr=($file_name)
			for x in "${arr[0]}";
			do
				filename=${arr[0]}
				filedate=${arr[1]}
				if  compare $filename $filedate; then
					IFS='\'
					moved_file="${filename}_${filedate}.xml"
					echo "Moved " $moved_file >> XmlSelectionScript.log
					mv $SEARCH_DIR/$moved_file $MOVE_DIR/$moved_file
				fi			
			done
		fi
	done
}
##############
# START SCRIPT
##############
echo -n "" > XmlSelectionScript.log

# start on 
echo "Start on $(date)" >> XmlSelectionScript.log

scan $SEARCH_DIR

# end on
echo "End on $(date)" >> XmlSelectionScript.log
echo "Check XmlSelectionScript.log for details"
