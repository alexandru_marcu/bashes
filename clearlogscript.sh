#!/bin/bash

# ====================================
# DEFINE DIRECTORY OF FILES TO DELETE
# ====================================

DataTV_BASEDIR='/product/cw/tools/DataTV/in/done'
ETENTION_DAYS_LOG=7
 
# ====================================
# DEFINE EXTESIONS OF FILES TO DELETE
# ====================================

EXTENSION_FIELD=(
  'ingestsss'
  'ingested'
  'ingestion'
  )
echo -n "" > cleanscriptDataTV.log

#cleanup searches in the directory for all the files with the declared extensions and deletes them
function cleanup {
  directory=$1
  extension=$2
  find $directory/*\.$extension  -exec echo "Deleting " {} >> cleanscriptDataTV.log \; -exec rm -f {} \;
}

# start on:
echo "Start cleaning on $(date)" >> cleanscriptDataTV.log

# clean folder
if [ -d $DataTV_BASEDIR ]; then
    for fld in "${EXTENSION_FIELD[@]}"; do
        cleanup $DataTV_BASEDIR $fld
    done
fi
# completed on:
echo "Cleaning complete on $(date)" >> cleanscriptDataTV.log
echo "Check cleanscriptDataTV.log for details"
