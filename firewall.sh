#!/bin/sh

#SERVER=localhost
SERVER=115.41.239.152

#Flushing all rules
iptables -F
iptables -X

#Setting default filter policy (for INPUT/OUTPUT chains)
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT

#Allow unlimited traffic on loopback
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Block Chinese/Bangladesh IPs trying to connect by ssh (found in /var/log/secure) - 22-12-2015
#iptables -A INPUT -p tcp -s 121.156.122.97/16   -d $SERVER --dport 22 -m state --state NEW -j LOG --log-level 4
#iptables -A INPUT -p tcp -s 210.73.74.224/16   -d $SERVER --dport 22 -m state --state NEW -j DROP
# Add more if you want

#Allow incoming ssh only
#allow ssh connections from cw hq
iptables -A INPUT -p tcp -d $SERVER -s 89.96.103.170 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
#allow ssh connections from Chase IP
iptables -A INPUT -p tcp -d $SERVER -s 113.199.55.24 -m state --state NEW,ESTABLISHED -j ACCEPT
#allow connnections on port 80 from 2nd screen app developers IPs
iptables -A INPUT -p tcp -d $SERVER -s 121.78.33.120 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -d $SERVER -s 121.78.33.121 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -d $SERVER -s 121.78.33.122 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -d $SERVER -s 121.78.33.123 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -d $SERVER -s 121.78.33.124 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -d $SERVER -s 121.78.33.125 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT

#allow connections on every port from other servers in the pool
iptables -A INPUT -p tcp -d $SERVER -s 115.41.239.153 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -d $SERVER -s 115.41.239.154 -m state --state NEW,ESTABLISHED -j ACCEPT
#allow every server as destination from this server
#iptables -A OUTPUT -p tcp -s $SERVER -d 0/0 --sport 22 -m state --state ESTABLISHED -j ACCEPT

#Allow ping
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT

#Allow outgoing connections
iptables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

#Allow comeback traffic
iptables -A INPUT -m state -s 0/0 --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT  -p tcp -m state --state ESTABLISHED -j ACCEPT

#allow database connection
#iptables -A OUTPUT -p tcp -d 115.41.239.154 --dport 3306 -j ACCEPT
#allow outgoing icmp
iptables -A OUTPUT -p icmp -j ACCEPT

#make sure that nothing else comes into this machine
#iptables -A INPUT -j DROP
#make sure nothing unwanted goes out
#iptables -A OUTPUT -j DROP

#iptables-save