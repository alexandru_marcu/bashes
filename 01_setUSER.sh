   #!/bin/bash
set -u
INPUT=$1
# wget http://ipinfo.io/ip -qO -
# 54.76.130.224

url=admin:admin@localhost/cuapi/user/CW_CALLER

IFS="|"
OLDIFS=$IFS
jsonPut=''
while read ServiceIn UserId Dummy UserType gender age subscribedPackagesArray ChannelArray
 do
	jsonPut='{
	"userId":"'$UserId'",
	"usertype":"'$UserType'",
	"metadata":[
		{
			"name":"Gender",
			"valueArray":[
			"'$gender'"
			]
		},
		{
			"name":"Age",
			"valueArray":[
			"'$age'"
			]
		},
		{
			"name":"SubscribedPackagesArray",
			"valueArray":[
			"'$subscribedPackagesArray'"
			]
		}
	]
}'
#echo "$jsonPut"	
curl --insecure -X PUT -H "Content-Type: application/json" --data $jsonPut $url 
	
 done < $INPUT
 IFS=$OLDIFS
