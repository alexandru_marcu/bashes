#!/bin/bash

# ############################################
# install standard CW libraries requirements
# ############################################

yum install -y blas
yum install -y blas-devel
yum install -y lapack
yum install -y lapack-devel
yum install -y gcc
yum install -y gcc-gfortran
yum install -y cpp
yum install -y gcc-c++
yum install -y glibc-devel
yum install -y blas
yum install -y glibc-static
yum install -y libstdc++-devel
yum install -y make
yum install -y python
yum install -y vim
yum install -y tar
yum install -y dos2unix
yum install -y unzip
yum install -y telnet
yum install -y wget
yum install -y rsync
yum install -y zip
yum install -y traceroute
yum install -y net-tools
yum install -y perl-Data-Dumper
yum install -y php
yum install -y php-pear
yum install -y php-mysql
yum install -y ntp