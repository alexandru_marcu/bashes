#!/bin/bash

# ############################################
# Check standard contentwise requirementes
# ############################################


function chkmsg {
        printf "\n\n"
        printf "*****************************************************************\n"
        printf "* $1\n"
        printf "*****************************************************************\n"
}


chkmsg 'Check release'
cat /etc/*release*

chkmsg 'Check arch bit'
getconf LONG_BIT

chkmsg 'Check memory (MB)'
free -m

chkmsg 'Check disk'
df -h

chkmsg 'Check Java'
java -version


chkmsg 'Check Apache'
rpm -qa | grep '^httpd-2.[24]' || echo httpd missing

chkmsg 'Check Zip'
rpm -qa | grep '^zip-[3-9]' || echo Zip 3+ missing

chkmsg 'Check python version'
python --version


chkmsg 'Check ulimit ( should be >8196)'
ulimit


chkmsg 'Check blas'
rpm -qa | grep '^blas-[3-9]' || echo blas missing

chkmsg 'Check libgomp'
rpm -qa | grep -P '^libgomp-(4.[4-9]|[4-9])' || echo libgomp missing

chkmsg 'Check lapack'
rpm -qa | grep '^lapack-[3-9]' || echo lapack missing


chkmsg 'Check sendmail'
rpm -qa | grep '^sendmail' || echo sendmail missing


FL=/proc/sys/vm/overcommit_memory
chkmsg "Overcommit. If not 1, run 'echo 1 > $FL'"
cat $FL

FL=/etc/sysctl.conf
chkmsg "Overcommit persistence. If missing, add 'vm.overcommit_memory = 1' in $FL"
grep 'vm.overcommit_memory' $FL || echo Value missing


chkmsg 'Check mysql'
rpm -qa | grep -P '^mysql-5.6.(1[7-9]|[2-9])' || echo mysql 5.6.x missing


Check 'Check SELinux. Edit /etc/selinux/config, run "echo 0 > /selinux/enforce"'
sestatus | grep 'SELinux status'