#!/bin/bash
set -u
INPUT=$1
# url=10.200.181.25/cuapi/userprofile/access/CABLEVISION_CALLER
url=admin:admin@localhost/cuapi/userprofile/access/CW_CALLER

IFS="|"
OLDIFS=$IFS
jsonPut=''

while read ServiceIn UserId UserType Viewed VisionFactor Duration ItemId ItemType RatingTime timeOffset Dummy Title
 do
	jsonPut='{
	"itemtype":"'$ItemType'",
	"serviceItemID":[
		{
			"serviceId":"'$ServiceIn'",
			"itemId":"'$ItemId'"
		}
	],
	"accessMetadata":[
		{
			"name":"Viewed",
			"valueArray":["'$Viewed'"]
		},
		{
			"name":"VisionFactor",
			"valueArray":["'$VisionFactor'"]
		}
	],
	"ratingTime":"'$RatingTime'",
	"timeOffset":"'$timeOffset'",
	"userId":"'$UserId'",
	"usertype":"'$UserType'"
}'
# echo $jsonPut
curl -X PUT -H "Content-Type: application/json" --data $jsonPut $url"?servicein=$ServiceIn"
	
done < $INPUT
IFS=$OLDIFS
