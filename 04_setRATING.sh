#!/bin/bash
set -u
INPUT=$1

url=admin:admin@localhost/cuapi/userprofile/rating/CW_CALLER

IFS="|"
OLDIFS=$IFS
jsonPut=''
while read ServiceIn UserId UserType Rating ItemId ItemType RatingTime timeOffset Title
 do
	jsonPut='{
	"rating":'$Rating',
	"itemtype":"'$ItemType'",
	"serviceItemID":[
		{
			"serviceId":"'$ServiceIn'",
			"itemId":"'$ItemId'"
		}
	],
	"ratingTime":"'$RatingTime'",
	"timeOffset":"'$timeOffset'",
	"userId":"'$UserId'",
	"usertype":"'$UserType'"
}'
# echo $jsonPut
curl -X PUT -H "Content-Type: application/json" --data $jsonPut $url"?servicein=$ServiceIn"
	
done < $INPUT
IFS=$OLDIFS
